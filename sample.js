/**
 * ARROW FUNCTION
 */

// CASE 1 -BEGINS
var createGreeting = function (message, name) {
  return message + name;
}

// example
var squared = x = x => x * x;
// CASE 1 - ENDS

// CASE 2 - BEGINS handling of "this"
  var deliveryBoy = {
  name: "John",
  handleMessage: function(message, handler) {
    handler(message);
  },
  receive: function() {
    var that = this;
    this.handleMessage("Hello", function(message) {
      that.name;
      console.log(message + that.name);
    });
  }
}
deliveryBoy.receive();
// CASE 2- ENDS

/**
 * USING "let"
 */
 // EXAMPLE 1
var message = "Hello";

{
  var message = "Hi";
}

function greeting() {
  var message = "Namaste";
}

console.log(message);

// EXAMPLE 2
var fs =  [];

for(var i=0; i<10; i++) {
  fs.push(function(){
    console.log(i);
  });
}

fs.forEach(function(f){
  f();
});

//EXAMPLE 3 - Fibonnachi series
function varFunc() {
  var previous = 0;
  var current = 1;
  var i;
  var temp;

  for(i=0; i<10; i++) {
    temp = previous;
    previous = current;
    current = temp + current;
  }
}
