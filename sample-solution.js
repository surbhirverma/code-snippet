// Arrow
//CASE 1
var createGreeting = (message, name) => message + name;

//CASE 2
var deliveryBoy = {
    name: "John",
    handleMessage: (message, handler) => handler(message),
    receive: function() {
    this.handleMessage("Hello, ", message => console.log(message + this.name));
  }
}
deliveryBoy.receive();

/**
 * USING "let" allows block scoping
*/
//EXAMPLE 1
let message = "Hello";
{
  let message = "Hi";
}
console.log(message);

//EXAMPLE 2
var fs =  [];

for(let i=0; i<10; i++) {
  fs.push(function(){
    console.log(i);
  });
}

fs.forEach(function(f){
  f();
});

module.exports.execute = function() {

//EXAMPLE 3 - Fibonnachi Series
function varFunc() {
  let previous = 0;
  let current = 1;

  for(let i=0; i<10;i++) {
    let temp = previous;
    previous = current;
    current = temp + current;
  }
}

// const
const a = 1;
const b = {a:1};
b.c = 2
}


// usage of elipses in rest parameter and spread operator
function blah(a,b,...args) {
  console.log(`${Array.isArray(args)} \n a - ${a} b - ${b} args - ${args}`);
}

blah(...[1,2],[1,2]);
